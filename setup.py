from setuptools import setup, find_packages
setup(
    # name of package
    name="snapshot",
    # packages (directories) to be included
    packages=find_packages(),
    # script entry point
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    # package dependencies
    install_requires=[
        "termcolor==1.1.0"
    ],
    version="0.1",
    author="Katsiaryna Yakuts",
    author_email="katsiaryna_yakuts@epam.com",
    description="Information about your system/server.",
    license="MIT")
