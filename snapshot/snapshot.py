"""
Make snapshot

{"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
"%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
"KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
"KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
"Timestamp": 1624400255}
"""
import argparse
import psutil
import time
import json
import os


class tasks(object):
    "Common information about process` status"
    s_sleep = s_zombie = s_stopped = s_running = 0

    def __init__(self, total):
        tasks.s_sleep = tasks.s_zombie = tasks.s_stopped = tasks.s_running = 0

        for key in total.values():
            if key["status"] == "sleeping":
                tasks.s_sleep += 1
            if key["status"] == "zombie":
                tasks.s_zombie += 1
            if key["status"] == "stopped":
                tasks.s_stopped += 1
            if key["status"] == "running":
                tasks.s_running += 1


def main():
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=30)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
    args = parser.parse_args()

    open(args.f, "w").close()
    os.system("clear")

    amount_of_sn = 0
    monitor = {}

    while amount_of_sn < int(args.n):
        total = {p.status: p.info for p in psutil.process_iter(["status"])}
        pp = tasks(total)

        monitor["Tasks"] = {
            "total": len(total),
            "running": pp.s_running,
            "sleeping": pp.s_sleep,
            "stopped": pp.s_stopped,
            "zombie": pp.s_zombie}
        monitor["%CPU"] = {
            "user": psutil.cpu_times_percent().user,
            "system": psutil.cpu_times_percent().system,
            "idle": psutil.cpu_times_percent().idle}
        monitor["KiB Mem"] = {
            "total": psutil.virtual_memory().total // 1024,
            "free": psutil.virtual_memory().free // 1024,
            "used": psutil.virtual_memory().used // 1024}
        monitor["KiB Swap"] = {
            "total": psutil.swap_memory().total // 1024,
            "free": psutil.swap_memory().free // 1024,
            "used": psutil.swap_memory().used // 1024}
        monitor["Timestamp"] = int(time.time())

        print(monitor, end="\r")

        amount_of_sn += 1

        file = open(args.f, "a")
        json.dump(monitor, file)
        file.write("\n")

        if amount_of_sn < int(args.n):
            time.sleep(args.i)


if __name__ == "__main__":
    main()
